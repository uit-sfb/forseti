# Forseti

Forseti is a delegate authentication service offering multiple authentication methods and identity consolidation.

Forseti is the god of justice in Norse mythology.

## About Forseti

The diagram below presents an overview of what is Forseti and how it can be used by client applications.

![Forseti overview](source/images/schematics/forseti_overview.png)

An application client contains a `Forseti client` implementing the [Forseti specification](https://gitlab.com/uit-sfb/forseti/tree/master/clients#forseti-specification).
A list of ready to use clients is available [here](https://gitlab.com/uit-sfb/forseti/tree/master/clients#list-of-clients).

When the application client detects an access to a protected URL, it redirects the user to a Forseti server for authentication.
The authentication process is composed of several steps:
  - the user chooses which identity provider it wishes to use
  - he logs-in with the selected provider
  - an aggregate identity is computed (or is created in case it is the first time the user logs in with this idp)
  - Forseti server returns the aggregate identity to the client
  - the application client continues its own security process with the authorization step, then allows the access of the URL if successful.
  
Note: Forseti only deals with authentication, NOT authorization.


## Sub-identities and Aggregated identity

Depending on the configuration of the Forseti server, several identity providers may be available for the user to choose from.
Logging in with a provider returns a sub-identity. The concept behind Forseti is that a sub-identity needs to be linked to an aggregated identity, which
is the identity Forseti returns to the client. In the diagram below, the user logged in with Facebook provider which returned the id `fff@facebook.org`.
This id belongs to the aggregated identity `bbb@forseti.aggregated.org` which is the one the client receives.

![Forseti_identiies](source/images/schematics/forseti_identities.png)

Identities are aggregated in the `forseti.identities` collection. Each time a user logs in with a provider, Forseti looks in
this collection if an identity contains the sub-id.
  - If so it simply returns the matching aggregated identity.
  - If not, it asks the user to create/log in to a Forseti account (which is stored in `forseti.credentials` collection), then create/update the aggregate identity in `forseti.identities`.
  
The identity returned by Forseti is not only an Id but a profile (id, name, email, ...). When a new sub-id is linked to an aggregated-identity Forseti may
set properties that are currently empty, but may NOT override already set properties.