# Forseti backend

## QuickStart

```bash
helm init --wait --history-max 200
helm install --wait --name mongodb stable/mongodb
. setenv.sh #Note that it is NOT `./setenv.sh`
kubectl port-forward --namespace default svc/mongodb 27017:27017 &
sbt run 9000
```

## Settings

### Identity providers

By default only Forseti authentication is proposed in the portal. However, it is possible to add more identity providers.

#### Elixir AAI

To use Elixir AAI, the configuration must contain `app.auth.elixirAai.discovery`.
If so, must be configured:
  - `app.auth.elixirAai.discovery`: url of OIDC provider metadata
  - `app.auth.elixirAai.client.id`: client id provided by your OIDC provider
  - `app.auth.elixirAai.client.secret`: client secret provided by your OIDC provider

### Database

A Mongo Db instance is used to store the identities and credentials.
Must be configured:
  - `db.mongodb.host`
  - `db.mongodb.port`
  - `db.mongodb.ssl`: set to true if mongodb is behind a reverse-proxy configured with ssl
  - `db.mongodb.user`
  - `db.mongodb.password`

### Other settings

  - `app.urlResolver.forceHttps`: set to true if the server is behind a reverse proxy

## Publish

To publish a docker image, run `docker:publish` (or `docker:publishLocal` for local testing).

## Deploy

Deploy as a standard Docker service (registry.gitlab.com/uit-sfb/forseti/forseti-backend:<version>).
To use the docker image, login with docker:
`docker login registry.gitlab.com -u gitlab+deploy-token-87693 -p 45C5xWWCMgjh_xivLktb`

You should mount the config file on `/app/app.conf`. Starting the file with `include "application"` is adviced.