import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.core.config.Configurator
import org.apache.logging.log4j.Level._
import com.typesafe.sbt.packager.docker.ExecCmd

name := "forseti-backend"
organization := "no.uit.sfb"
logLevel := {
  Configurator.setAllLevels(LogManager.getRootLogger.getName, INFO)
  sbt.util.Level.Info
}

ThisBuild / resolvers ++= {
  Seq(Some("Artifactory" at "https://artifactory.metapipe.uit.no/artifactory/sbt-release-local/"),
    if ((ThisBuild / version).value.endsWith("-SNAPSHOT"))
      Some("Artifactory-dev" at "https://artifactory.metapipe.uit.no/artifactory/sbt-dev-local/")
    else
      None
  ).flatten
}

lazy val root = (project in file(".")).enablePlugins(PlayScala, SwaggerPlugin)

enablePlugins(GitVersioning)
useJGit
git.gitlabCiOverride := true
git.targetVersionFile := "../targetVersion"

scalaVersion := "2.12.8"
val playVersion = "2.7.1"
val pac4jVersion = "3.7.0"

libraryDependencies ++= Seq(
  guice,
  "com.typesafe.play" %% "play-json" % playVersion,
  "org.webjars" % "swagger-ui" % "3.22.0",
  "com.typesafe.play" %% "play-mailer" % "7.0.0",
  "com.typesafe.play" %% "play-mailer-guice" % "7.0.0",
  "org.scalatestplus.play" %% "scalatestplus-play" % "4.0.1" % Test,
  "org.pac4j" %% "play-pac4j" % "8.0.0",
  "org.pac4j" % "pac4j-oidc" % pac4jVersion exclude("commons-io" , "commons-io"),
  "org.pac4j" % "pac4j-http" % pac4jVersion,
  "org.pac4j" % "pac4j-mongo" % pac4jVersion,
  "org.apache.shiro" % "shiro-core" % "1.4.0",
  "com.typesafe.play" %% "play-cache" % playVersion
)

swaggerDomainNameSpaces := Seq("app/models")

val pubDockerRepository = "registry.gitlab.com"
val pubDockerUsername = "uit-sfb/forseti"

dockerLabels := Map("gitCommit" -> s"${git.formattedShaVersion.value.getOrElse("unknown")}@${git.gitCurrentBranch.value}")
dockerRepository in Docker := Some(pubDockerRepository)
dockerUsername in Docker := Some(pubDockerUsername)
dockerCommands := dockerCommands.value.map {
  // ExecCmd is a case class, and args is a varargs variable, so you need to bind it with @
  case ExecCmd("CMD", args @ _*) => ExecCmd("CMD", (args ++ Seq("-Dconfig.file=/app/app.conf")):_*)

  // don't filter the rest; don't filter out anything that doesn't match a pattern
  case cmd                       => cmd
}

enablePlugins(BuildInfoPlugin)
buildInfoKeys := Seq[BuildInfoKey](name, version, scalaVersion, sbtVersion)
buildInfoPackage := s"${organization.value}.info.${name.value.replace('-', '_')}"
