package models

import java.util.UUID

import play.api.libs.json.Json

case class Identity(
    id: String = UUID.randomUUID().toString ++ "@forseti.aggregate.org",
    username: String,
    linkedIds: Map[String, String],
    email: String = "",
    name: String = "")

object Identity {
  implicit val format = Json.format[Identity]
}
