package models

import org.pac4j.mongo.profile.MongoProfile
import play.api.libs.json.Json

import scala.collection.JavaConverters._

case class Credentials(
    username: String,
    password: String
) {
  lazy val asMongoProfile = {
    val profile = new MongoProfile()
    profile.build(s"$username@forseti.org",
                  Map[String, AnyRef]("username" -> username,
                                      "password" -> password).asJava)
    profile
  }
}

object Credentials {
  implicit val format = Json.format[Credentials]
}
