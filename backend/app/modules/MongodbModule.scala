package modules

import java.util.concurrent.TimeUnit

import com.mongodb.{MongoClient, MongoClientOptions, ServerAddress}
import com.mongodb.client.MongoCollection
import com.mongodb.client.model.{Filters, IndexOptions, Indexes}
import javax.inject.{Inject, Singleton}
import org.bson.Document
import play.api.Configuration
import play.api.libs.json.{Json, Reads, Writes}
import com.mongodb.BasicDBObject

import scala.concurrent.duration._
import scala.collection.JavaConverters._

@Singleton
class MongodbModule @Inject()(config: Configuration) {

  import com.mongodb.MongoCredential

  val databaseName = "forseti"

  object Collections {

    /**
      * Collection of codes -> identities id
      */
    val accessCodes = {
      val coll = collection("accessCodes")
      createTtl(coll, 10.seconds)
      coll
    }

    /**
      * Collection of identities
      */
    val identities = {
      val coll = collection("identities")
      coll.createIndex(Indexes.hashed("id"))
      coll
    }

    /**
      * Collection of local credentials
      */
    val credentials = {
      val coll = collection("credentials")
      coll.createIndex(Indexes.hashed("username"))
      coll
    }

    /**
      * Collection of clients credentials
      */
    val clients = {
      val coll = collection("clients")
      coll.createIndex(Indexes.hashed("clientname"))
      coll
    }
  }

  val client = {
    val credential = MongoCredential.createCredential(
      config.get[String]("db.mongodb.user"),
      "admin",
      config.get[String]("db.mongodb.password").toCharArray)
    new MongoClient(
      new ServerAddress(config.get[String]("db.mongodb.host"),
                        config.get[Int]("db.mongodb.port")),
      credential,
      MongoClientOptions
        .builder()
        .socketTimeout(30000)
        .maxConnectionIdleTime(5000)
        .sslEnabled(config.get[Boolean]("db.mongodb.ssl"))
        .build()
    )
  }

  /**
    * Return the collection matching the coordinates
    * @param db db name
    * @param coll collection name
    * @return
    */
  protected def collection(
      coll: String,
      db: String = databaseName): MongoCollection[Document] = {
    assert(coll.nonEmpty, "Collection name should be non-empty")
    assert(db.nonEmpty, "Database name should be non-empty")
    client.getDatabase(db).getCollection(coll)
  }

  /**
    * Create a ttl index
    * @param coll collection
    * @param expiresAfter  ttl
    * @param indexName index name
    * @return
    */
  protected def createTtl(coll: MongoCollection[Document],
                          expiresAfter: Duration,
                          indexName: String = "createdAt"): Unit = {
    coll.createIndex(
      Indexes.ascending(indexName),
      new IndexOptions().expireAfter(expiresAfter.toSeconds, TimeUnit.SECONDS)
    )
  }

  protected def stringify[T](obj: T)(implicit write: Writes[T]): String =
    Json.stringify(Json.toJson(obj))

  /**
    * Throws JsResultException if parsing fails
    * @param doc
    * @param read
    * @tparam T
    * @return
    */
  protected def as[T](doc: Document)(implicit read: Reads[T]): T =
    Json.parse(doc.toJson()).as[T]

  def insert(coll: MongoCollection[Document],
             data: Map[String, AnyRef]): Unit = {
    import scala.collection.JavaConverters._
    val doc = new Document(data.asJava)
    coll.insertOne(doc)
  }

  /** Insert a new document
    *
    * @param coll
    * @param data data to insert (string)
    * @return the automatic _id
    */
  def insert(coll: MongoCollection[Document], data: String): String = {
    val doc = Document.parse(data)
    coll.insertOne(doc)
    doc.getObjectId("_id").toHexString
  }

  /** Insert a new document
    *
    * @param coll
    * @param data data to insert (object of type T)
    * @param write
    * @tparam T type of the data
    * @return the automatic _id
    */
  def insert[T](coll: MongoCollection[Document], data: T)(
      implicit write: Writes[T]): String = {
    val str = Json.stringify(Json.toJson(data))
    insert(coll, str)
  }

  def findOneWithField(coll: MongoCollection[Document],
                       value: AnyRef,
                       indexName: String = "_id"): Option[Document] = {
    coll.find(Filters.eq(indexName, value)).asScala.headOption
  }

  def findOneWithFieldAs[T](
      coll: MongoCollection[Document],
      value: AnyRef,
      indexName: String = "_id")(implicit read: Reads[T]): Option[T] = {
    findOneWithField(coll, value, indexName) map as[T]
  }

  def findOneAndDeleteWithField(coll: MongoCollection[Document],
                                value: AnyRef,
                                indexName: String = "_id"): Option[Document] = {
    Option(coll.findOneAndDelete(Filters.eq(indexName, value)))
  }

  def findOneAndDeleteWithFieldAs[T](
      coll: MongoCollection[Document],
      value: AnyRef,
      indexName: String = "_id")(implicit read: Reads[T]): Option[T] = {
    findOneAndDeleteWithField(coll, value, indexName) map as[T]
  }

  def update(coll: MongoCollection[Document],
             indexValue: AnyRef,
             indexName: String,
             map: Map[String, AnyRef]): Unit = {
    val doc = new BasicDBObject("$set", new BasicDBObject(map.asJava))
    coll.updateOne(Filters.eq(indexName, indexValue), doc)
  }

  def replace(coll: MongoCollection[Document],
              value: AnyRef,
              indexName: String,
              replacement: String): Unit = {
    val docReplacement = Document.parse(replacement)
    coll.replaceOne(Filters.eq(indexName, value), docReplacement)
  }

  def replace[T](coll: MongoCollection[Document],
                 value: AnyRef,
                 indexName: String,
                 replacement: T)(implicit write: Writes[T]): Unit = {
    replace(coll, value, indexName, stringify(replacement))
  }
}
