package modules

import javax.inject.{Inject, _}
import play.api.libs.mailer.MailerClient
import play.api.{Configuration, Logging}
import play.api.inject._

@Singleton
class EmailModule @Inject()(mailerClient: MailerClient, config: Configuration)
    extends Logging {}
