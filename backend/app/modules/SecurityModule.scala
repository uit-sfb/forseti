package modules

import com.google.inject.{AbstractModule, Provides}
import controllers.routes
import org.pac4j.core.client.Clients
import org.pac4j.oidc.client.OidcClient
import org.pac4j.play.{CallbackController, LogoutController}
import org.apache.shiro.authc.credential.DefaultPasswordService
import org.pac4j.play.store.{
  PlayCookieSessionStore,
  PlaySessionStore,
  ShiroAesDataEncrypter
}
import org.pac4j.core.config.Config
import org.pac4j.core.credentials.password.{
  PasswordEncoder,
  ShiroPasswordEncoder
}
import org.pac4j.core.profile.CommonProfile
import org.pac4j.http.client.direct.DirectBasicAuthClient
import org.pac4j.http.client.indirect.FormClient
import org.pac4j.mongo.profile.service.MongoProfileService
import org.pac4j.oidc.config.OidcConfiguration
import org.pac4j.oidc.profile.OidcProfile
import org.pac4j.play.http.PlayHttpActionAdapter
import org.pac4j.play.scala.{
  DefaultSecurityComponents,
  Pac4jScalaTemplateHelper,
  SecurityComponents
}
import play.api.{Configuration, Environment}
import utils.auth.authorizers.MongoProfileServiceExt
import utils.auth.helpers.CustomUrlResolver

/**
  * Guice DI module to be included in application.conf
  */
class SecurityModule(environment: Environment, configuration: Configuration)
    extends AbstractModule {

  override def configure(): Unit = {

    val sKey =
      configuration.get[String]("play.http.secret.key").substring(0, 16)
    val dataEncrypter = new ShiroAesDataEncrypter(sKey)
    val playSessionStore = new PlayCookieSessionStore(dataEncrypter)
    bind(classOf[PlaySessionStore]).toInstance(playSessionStore)

    bind(classOf[SecurityComponents]).to(classOf[DefaultSecurityComponents])

    bind(classOf[Pac4jScalaTemplateHelper[CommonProfile]]) //??

    // callback
    val callbackController = new CallbackController()
    callbackController.setRenewSession(false)
    callbackController.setMultiProfile(true)
    bind(classOf[CallbackController]).toInstance(callbackController)

    // logout
    val logoutController = new LogoutController()
    logoutController.setDefaultUrl("/")
    logoutController.setLogoutUrlPattern(".*")
    bind(classOf[LogoutController]).toInstance(logoutController)
  }

  /** Elixir AAI Client (Indirect OIDC)
    * Client application -> Forseti
    */
  @Provides
  def provideElixirAaiClient
    : Option[OidcClient[OidcProfile, OidcConfiguration]] = {
    configuration.getOptional[String]("app.auth.elixirAai.discovery") map {
      metadata =>
        val oidcConfiguration = new OidcConfiguration()
        //oidcConfiguration.setScope("openid profile email")
        oidcConfiguration.setClientId(
          configuration.get[String]("app.auth.elixirAai.client.id"))
        oidcConfiguration.setSecret(
          configuration.get[String]("app.auth.elixirAai.client.secret"))
        oidcConfiguration.setDiscoveryURI(metadata)
        oidcConfiguration.addCustomParam("prompt", "consent")
        val oidcClient =
          new OidcClient[OidcProfile, OidcConfiguration](oidcConfiguration)
        oidcClient.setName("ElixirAaiClient")
        oidcClient
    }
  }

  /** Backend Basic Client (Direct Basic)
    * Client application -> Backend
    * This is used to authentify an application and not a user
    */
  @Provides
  def provideClientBackendDirectBasicAuthClient(
      passwordEncoder: PasswordEncoder): DirectBasicAuthClient = {
    val mongoDbModule = provideMongodbModule
    val mongoClient = mongoDbModule.client
    val mongoProfileService =
      new MongoProfileService(mongoClient, "", passwordEncoder)
    mongoProfileService.setIdAttribute("_id")
    mongoProfileService.setUsernameAttribute("clientname")
    mongoProfileService.setUsersCollection(
      mongoDbModule.Collections.clients.getNamespace.getCollectionName)
    mongoProfileService.setUsersDatabase(
      mongoDbModule.Collections.clients.getNamespace.getDatabaseName)
    val localClient = new DirectBasicAuthClient(mongoProfileService)
    localClient.setName("ClientBackendDirectClient")
    localClient
  }

  @Provides
  def providePasswordEncoder: PasswordEncoder = {
    new ShiroPasswordEncoder(new DefaultPasswordService())
  }

  @Provides
  def provideMongoProfileService(
      passwordEncoder: PasswordEncoder): MongoProfileServiceExt = {
    val mongoDbModule = provideMongodbModule
    val mongoClient = mongoDbModule.client
    val mongoProfileService =
      new MongoProfileServiceExt(
        mongoClient,
        Seq(""),
        Seq("id"), //It has to be "id" because otherwise the remove method does not work!!
        passwordEncoder)
    mongoProfileService.setIdAttribute("_id")
    mongoProfileService.setUsersCollection(
      mongoDbModule.Collections.credentials.getNamespace.getCollectionName)
    mongoProfileService.setUsersDatabase(
      mongoDbModule.Collections.credentials.getNamespace.getDatabaseName)
    mongoProfileService
  }

  /** Form Client (Indirect Form)
    * Client application -> Forseti
    */
  @Provides
  def provideFormClient(
      mongoProfileService: MongoProfileServiceExt): FormClient = {
    val client =
      new FormClient(routes.AuthController.loginForm(None, None).url,
                     mongoProfileService)
    client.setName("LocalClient")
    client
  }

  @Provides
  def provideMongodbModule: MongodbModule = new MongodbModule(configuration)

  @Provides
  def provideConfig(
      elixirAaiClient: Option[OidcClient[OidcProfile, OidcConfiguration]],
      formClient: FormClient,
      directBasicAuthClient: DirectBasicAuthClient): Config = {
    val cList = Seq(elixirAaiClient,
                    Some(directBasicAuthClient),
                    Some(formClient)).flatten
    val clients = new Clients(
      "/callback", //The callback will be baseUrl/callback?client_name=<clientName>
      cList: _*
    )
    clients.setUrlResolver(
      new CustomUrlResolver(configuration.get[Boolean](
        "app.urlResolver.forceHttps"))) //When Forseti is behind a reverse proxy, it receives the urls as http instead of https. We have to force the url resolver to use https to get the right callbacks.
    val config = new Config(clients)
    config.setHttpActionAdapter(new PlayHttpActionAdapter())
    config
  }
}
