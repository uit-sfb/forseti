package utils.customresults

import play.api.mvc.Result
import play.api.mvc.Results._

object ServerErrors {
  def ISE(message: String = ""): Result =
    InternalServerError(
      s"Oops, the server encountered an unexpected error and cannot serve your request${if (message.nonEmpty)
        s": $message"}")
}
