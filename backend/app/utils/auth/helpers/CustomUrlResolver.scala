package utils.auth.helpers

import org.pac4j.core.context.WebContext
import org.pac4j.core.http.url.DefaultUrlResolver

class CustomUrlResolver(forceHttps: Boolean) extends DefaultUrlResolver(true) {
  override def compute(url: String, context: WebContext): String = {
    val str = super.compute(url, context)
    if (forceHttps && str.startsWith("http://"))
      s"https${str.drop(4)}"
    else
      str
  }
}
