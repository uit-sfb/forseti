package utils.auth.profile_creators

import org.pac4j.core.context.WebContext
import org.pac4j.oidc.config.OidcConfiguration
import org.pac4j.oidc.credentials.OidcCredentials
import org.pac4j.oidc.profile.OidcProfile
import org.pac4j.oidc.profile.creator.OidcProfileCreator
import utils.auth.authorizers.MongoProfileServiceExt

class ElixirAaiProfileCreator[U <: OidcProfile](
    cfg: OidcConfiguration,
    mongoProfileService: MongoProfileServiceExt)
    extends OidcProfileCreator[U](cfg) {
  override def create(credentials: OidcCredentials, context: WebContext): U = {
    import scala.collection.JavaConverters._
    //It throws if the profile cannot be found
    val profile = super.create(credentials, context)
    val matchingProfile = mongoProfileService
      .findByAttr("elixir-aai-id", profile.getSubject)
      .headOption
    matchingProfile match {
      case Some(p) => //TODO: update info
      case None =>
        val filteredAttributes = profile.getAttributes.asScala.toMap flatMap {
          case ("sub", v)                => Some("elixir-aai-id" -> v)
          case ("preferred_username", v) => Some("username" -> v)
          case (k, v) if Seq("name", "email").contains(k) =>
            Some(k -> v)
          case _ => None
        }
        mongoProfileService.insert(filteredAttributes.asJava)
    }
    profile
  }
}
