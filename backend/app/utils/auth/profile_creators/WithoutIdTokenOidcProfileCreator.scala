package utils.auth.profile_creators

import com.nimbusds.jwt.SignedJWT
import com.nimbusds.oauth2.sdk.token.{BearerAccessToken, RefreshToken}
import com.nimbusds.openid.connect.sdk.{
  Nonce,
  UserInfoErrorResponse,
  UserInfoRequest,
  UserInfoResponse,
  UserInfoSuccessResponse
}
import org.pac4j.core.context.WebContext
import org.pac4j.core.profile.AttributeLocation.PROFILE_ATTRIBUTE
import org.pac4j.core.profile.ProfileHelper
import org.pac4j.core.profile.jwt.JwtClaims
import org.pac4j.core.util.CommonHelper.assertNotNull
import org.pac4j.oidc.config.OidcConfiguration
import org.pac4j.oidc.credentials.OidcCredentials
import org.pac4j.oidc.profile.OidcProfile
import org.pac4j.oidc.profile.creator.OidcProfileCreator
import org.slf4j.LoggerFactory

/**
  * Same as OidcProfileCreator but tolerate when id_token is missing
  * Note: configuration.isUseNonce == true do not take effect!
  */
class WithoutIdTokenOidcProfileCreator[U <: OidcProfile](cfg: OidcConfiguration)
    extends OidcProfileCreator[U](cfg) {
  private val logger =
    LoggerFactory.getLogger(classOf[WithoutIdTokenOidcProfileCreator[_]])

  override def create(credentials: OidcCredentials, context: WebContext): U = {
    init()
    val accessToken = credentials.getAccessToken

    // Create profile
    val profile = getProfileDefinition.newProfile()
    profile.setAccessToken(accessToken)
    val idToken = credentials.getIdToken
    if (idToken != null)
      profile.setIdTokenString(idToken.getParsedString)
    // Check if there is a refresh token
    val refreshToken: RefreshToken = credentials.getRefreshToken
    if (refreshToken != null && !refreshToken.getValue.isEmpty) {
      profile.setRefreshToken(refreshToken)
      logger.debug("Refresh Token successful retrieved")
    }

    val nonce: Nonce =
      if (configuration.isUseNonce) {
        logger.warn(s"Nonce cannot be used for the moment")
        null
        //TODO: does not compile
        /*val store = context.getSessionStore.
        new Nonce(
          store
            .get(context, OidcConfiguration.NONCE_SESSION_ATTRIBUTE)
            .asInstanceOf[String])*/
      } else
        null

    // Check ID Token
    if (idToken != null) {
      val claimsSet = tokenValidator.validate(idToken, nonce)
      assertNotNull("claimsSet", claimsSet)
      profile.setId(
        ProfileHelper.sanitizeIdentifier(profile, claimsSet.getSubject))
    } else {
      //TODO: replace with elixir ID when userinfo works
      //If no id_token we set the id with sub from access_token
      val access_jwt = SignedJWT.parse(accessToken.getValue)
      val sub = access_jwt.getJWTClaimsSet.getSubject
      profile.setId(sub)
    }

    // User Info request
    if (configuration.findProviderMetadata.getUserInfoEndpointURI != null && accessToken != null) {
      val userInfoRequest = new UserInfoRequest(
        configuration.findProviderMetadata.getUserInfoEndpointURI,
        accessToken.asInstanceOf[BearerAccessToken])
      val userInfoHttpRequest = userInfoRequest.toHTTPRequest
      userInfoHttpRequest.setConnectTimeout(configuration.getConnectTimeout)
      userInfoHttpRequest.setReadTimeout(configuration.getReadTimeout)
      val httpResponse = userInfoHttpRequest.send
      logger.debug("Token response: status={}, content={}",
                   httpResponse.getStatusCode,
                   httpResponse.getContent)
      val userInfoResponse = UserInfoResponse.parse(httpResponse)
      userInfoResponse match {
        case _: UserInfoErrorResponse =>
          logger.error(
            "Bad User Info response, error={}",
            userInfoResponse.asInstanceOf[UserInfoErrorResponse].getErrorObject)
        case _ =>
          val userInfoSuccessResponse =
            userInfoResponse.asInstanceOf[UserInfoSuccessResponse]
          val userInfoClaimsSet =
            if (userInfoSuccessResponse.getUserInfo != null)
              userInfoSuccessResponse.getUserInfo.toJWTClaimsSet
            else
              userInfoSuccessResponse.getUserInfoJWT.getJWTClaimsSet
          getProfileDefinition.convertAndAdd(profile,
                                             userInfoClaimsSet.getClaims,
                                             null)
      }
    }

    // add attributes of the ID token if they don't already exist
    import scala.collection.JavaConverters._
    if (idToken != null) {
      for (entry <- idToken.getJWTClaimsSet.getClaims.entrySet.asScala) {
        val key = entry.getKey
        val value = entry.getValue
        // it's not the subject and this attribute does not already exist, add it
        if (!(JwtClaims.SUBJECT == key) && profile.getAttribute(key) == null)
          getProfileDefinition.convertAndAdd(profile,
                                             PROFILE_ATTRIBUTE,
                                             key,
                                             value)
      }
    }

    // session expiration with token behavior
    profile.setTokenExpirationAdvance(configuration.getTokenExpirationAdvance)

    profile
  }
}
