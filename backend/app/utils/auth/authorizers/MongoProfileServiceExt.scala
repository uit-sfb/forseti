package utils.auth.authorizers

import java.util

import com.mongodb.MongoClient
import org.pac4j.core.context.Pac4jConstants
import org.pac4j.core.credentials.password.PasswordEncoder
import org.pac4j.core.exception.TechnicalException
import org.pac4j.core.profile.ProfileHelper
import org.pac4j.mongo.profile.MongoProfile
import org.pac4j.mongo.profile.service.MongoProfileService

class MongoProfileServiceExt(
    mongoClient: MongoClient,
    attributes: Seq[String],
    uniques: Seq[String], //Attributes that must be unique
    passwordEncoder: PasswordEncoder)
    extends MongoProfileService(mongoClient,
                                attributes.mkString(","),
                                passwordEncoder) {
  override def insert(attributes: util.Map[String, Object]): Unit = {
    init()

    import scala.collection.JavaConverters._

    val attrs: Map[String, Object] = attributes.asScala.toMap

    uniques foreach { attr =>
      attrs.get(attr).map { _.asInstanceOf[String] } match {
        case Some(value) =>
          val listAttributes: util.List[util.Map[String, Object]] =
            read(defineAttributesToRead, attr, value)
          if (!listAttributes.isEmpty)
            throw new Exception(
              s"Another account already exists with this $attr ($value)")
        case _ => //nothing
      }
    }

    super.insert(attributes)
  }

  def findByAttr(attr: String, value: String): Seq[MongoProfile] = {
    import scala.collection.JavaConverters._
    init()
    val listAttributes = read(defineAttributesToRead, attr, value)
    listAttributes.asScala flatMap { u =>
      Option(convertAttributesToProfile(Seq(u).asJava, null))
    }
  }

  //Remove me when #1350 is merged!
  override def convertAttributesToProfile(
      listStorageAttributes: util.List[util.Map[String, AnyRef]],
      username: String) = {
    val profile =
      super.convertAttributesToProfile(listStorageAttributes, username)
    profile.addAttribute(Pac4jConstants.USERNAME, profile.getId)
    val storageAttributes: util.Map[String, AnyRef] =
      listStorageAttributes.get(0)
    val retrievedId = storageAttributes.get(getIdAttribute)
    if (retrievedId != null)
      profile.setId(ProfileHelper.sanitizeIdentifier(profile, retrievedId))
    else
      throw new TechnicalException("Could not retrieve id") //Should not happen
    profile
  }
}
