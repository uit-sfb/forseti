package controllers.forms

object NewUserForm {
  import play.api.data.Form
  import play.api.data.Forms._

  case class Data(
      username: String = "",
      password: String = "",
      passwordConfirm: String = "",
      email: String = ""
  )

  protected val minChars = 6

  val form = Form(
    mapping(
      "User name" -> nonEmptyText,
      "Password" -> nonEmptyText,
      "Password (confirm)" -> nonEmptyText,
      "Email" -> email
    )(Data.apply)(Data.unapply)
      .verifying(
        "Password confirm must match password!",
        fields => fields.password == fields.passwordConfirm
      )
      .verifying("Username must contain only alphanumerical and _ characters.",
                 fields => fields.username.matches("\\w*"))
      .verifying(s"Username must contain at least $minChars characters.",
                 fields => fields.username.matches(s".{$minChars,}"))
  )
}
