package controllers

import java.net.URLEncoder
import java.util.{Date, UUID}

import javax.inject._
import org.pac4j.core.profile.CommonProfile
import org.pac4j.http.client.indirect.FormClient
import org.pac4j.play.scala.{
  Pac4jScalaTemplateHelper,
  Security,
  SecurityComponents
}
import play.api._
import play.api.mvc._
import forms.LoginForm
import forms.NewUserForm
import models.{Credentials, Identity}
import modules.MongodbModule
import org.pac4j.core.credentials.UsernamePasswordCredentials
import org.pac4j.play.PlayWebContext
import utils.auth.authorizers.MongoProfileServiceExt
import utils.customresults.ServerErrors

import scala.util.{Failure, Success, Try}

/**
  * Authorization controller
  */
@Singleton
class AuthController @Inject()(
    mongo: MongodbModule,
    messagesAction: MessagesActionBuilder,
    mongoProfileService: MongoProfileServiceExt,
    val controllerComponents: SecurityComponents,
    implicit val pac4j: Pac4jScalaTemplateHelper[CommonProfile],
    implicit val cfg: play.api.Configuration)
    extends Security[CommonProfile] {
  val logger = Logger(this.getClass)

  import scala.collection.JavaConverters._
  val clients = config.getClients.findAllClients().asScala.map { _.getName }

  val defaultClient = "LocalClient"
  val accessCodeFieldName = "access_code"

  /** Starting point of the authentication process
    *
    * @param callback url the was being accessed when the security hook was triggered
    * @return
    */
  def portal(callback: String) = messagesAction {
    implicit request: MessagesRequestHeader =>
      pac4j.getCurrentProfile match {
        case Some(p) =>
          logger.info(s"Logged in: $p")
          doCallback(callback)
        case None => Ok(views.html.portal(callback))
      }
  }

  /** Given a sub-id (ex: local id, AAI id, ...), find the matching consolidated id (if any)
    *
    * @param subId sub-id
    * @return consolidated id if exists
    */
  private def findConsolidatedId(subId: String): Option[Identity] = {
    (clients flatMap { client =>
      mongo.findOneWithFieldAs[Identity](mongo.Collections.identities,
                                         subId,
                                         s"linkedIds.$client")
    }).headOption
  }

  /** Generate random access code to be exchanged against a consolidated identity
    * The access code is posted on mongo.Collections.accessCodes.
    * Note: throw Exception when sub-id not found in any identities
    *
    * @param request
    *  @return access code
    */
  private def createAccessCode()(implicit request: RequestHeader): String = {
    val oId = findConsolidatedId(pac4j.getCurrentProfile.get.getId)
    oId match {
      case Some(identity) =>
        val accessCode = URLEncoder.encode(UUID.randomUUID().toString, "utf-8")
        mongo.insert(mongo.Collections.accessCodes,
                     Map(
                       "createdAt" -> new Date(),
                       "code" -> accessCode,
                       "id" -> identity.id
                     ))
        accessCode
      case None =>
        logger.warn(
          s"No consolidated identity found for sub-id '${pac4j.getCurrentProfile.get.getId}'")
        throw new Exception("Sub-id not found")
    }
  }

  /** Compute return URL by appending the parameter 'accessCodeFieldName to the origin URL
    *
    * @param originUrl url the was being accessed when the security hook was triggered
    * @param accessCode access code
    * @return
    */
  private def returnUrl(originUrl: String, accessCode: String): String = {
    def appendParamsToUrl(url: String, params: Map[String, String]): String = {
      val urlWithoutFragments = url.split('#').head
      val fragments = url.split('#').tail
      val char =
        if (urlWithoutFragments.contains('?'))
          "&"
        else "?"
      val extraParams = (params
        .map {
          case (k, v) =>
            s"${URLEncoder
              .encode(k, "utf-8")}=${URLEncoder.encode(v, "utf-8")}"
        })
        .mkString("&")
      (Seq(urlWithoutFragments ++ char ++ extraParams) ++ fragments)
        .mkString("#")
    }
    appendParamsToUrl(originUrl, Map(accessCodeFieldName -> accessCode))
  }

  /** Hook for launching the defaultClient authentication process
    *
    * @param originUrl url the was being accessed when the security hook was triggered
    * @return
    */
  def login(originUrl: String) = Secure(defaultClient).andThen(messagesAction) {
    implicit request: MessagesRequestHeader =>
      doCallback(originUrl)
  }

  /** Login form
    *
    * @param originUrl url the was being accessed when the security hook was triggered
    * @param client which client triggered the login process (in case of identity consolidation). None if defaultClient was the origin
    * @return
    */
  def loginForm(originUrl: Option[String] = None,
                client: Option[String] = None) =
    messagesAction { implicit request: MessagesRequestHeader =>
      Ok(
        views.html.loginForm(LoginForm.form,
                             originUrl.getOrElse(
                               config.getClients
                                 .findClient(defaultClient)
                                 .asInstanceOf[FormClient]
                                 .getCallbackUrl),
                             client)
      )
    }

  /** Hook for launching the ElixirAAI authentication process
    *
    * @param originUrl url the was being accessed when the security hook was triggered
    * @return
    */
  def elixiraai(originUrl: String) = {
    val provider = "ElixirAaiClient"
    Secure(provider).andThen(messagesAction) {
      implicit request: MessagesRequestHeader =>
        val webContext = new PlayWebContext(request, playSessionStore)
        //We check if an identity with this elixir id already exists
        val oElixiraai =
          mongo.findOneWithFieldAs[Identity](mongo.Collections.identities,
                                             pac4j.getCurrentProfile.get.getId,
                                             s"linkedIds.$provider")
        oElixiraai match {
          case Some(_) => //It already exists: follow the redirect
            doCallback(originUrl)
          case None => //It does not exist: create new local account
            Ok(
              views.html
                .linkAccounts(originUrl, provider))
        }
    }
  }

  /** Callback URL
    *
    * @return
    */
  def callbackRedirect() = Action { implicit request =>
    Redirect("/callback")
  }

  def reinitPassword() = TODO

  /** Register new account
    *
    * @param originUrl url the was being accessed when the security hook was triggered
    * @param client which client triggered the registration process (in case of identity consolidation). None if user creates account manually.
    * @return
    */
  def register(originUrl: String, client: Option[String] = None) =
    messagesAction { implicit request: MessagesRequestHeader =>
      val userForm = client match {
        case Some(_) =>
          val current = pac4j.getCurrentProfile.get
          NewUserForm.form.fill(
            NewUserForm.Data(username = current.getUsername,
                             email = current.getEmail))
        case None => NewUserForm.form
      }
      Ok(views.html.registerForm(userForm, originUrl, client))
    }

  /** Starts up process of identity consolidation
    *
    * @param originUrl url the was being accessed when the security hook was triggered
    * @param client which client triggered the identity consolidation process.
    * @return
    */
  def linkAccounts(originUrl: String, client: String) = messagesAction {
    implicit request: MessagesRequestHeader =>
      Ok(views.html.linkAccounts(originUrl, client))
  }

  /** Append identity to database
    *
    * @param username
    * @param password
    * @param email
    * @param name
    * @param providerId
    * @return
    */
  protected def createIdentity(
      username: String,
      password: String,
      email: String,
      name: String = "",
      providerId: Option[(String, String)] = None): Identity = {
    //Todo: should fail in case of conflict
    //TODO: Confirm email before creating account!
    //We check that the username does no contain @ (which is used to segregate the ids)
    assert(!username.contains("@"),
           "A username cannot contain reserved char '@'.")
    //We create the credentials object and insert it to the credentials collection
    val credentials = Credentials(username, password)
    val mongoProfile = credentials.asMongoProfile
    mongoProfileService.create(mongoProfile, password)
    val identity =
      Identity(
        username = username,
        linkedIds = Map(defaultClient -> mongoProfile.getId) ++ (providerId match {
          case Some((client, subId)) => Map(client -> subId)
          case None                  => Map()
        }),
        email = email,
        name = name
      )
    logger.info(
      s"Insert new identity '$username' in ${mongo.Collections.identities.getNamespace}")
    mongo.insert(mongo.Collections.identities, identity)
    identity
  }

  /** Form processing for user registration
    *
    * @param originUrl url the was being accessed when the security hook was triggered
    * @param client which client triggered the registration process (in case of identity consolidation). None if user creates account manually.
    * @return
    */
  def registerSubmit(originUrl: String, client: Option[String] = None) =
    messagesAction { implicit request =>
      NewUserForm.form.bindFromRequest.fold(
        formWithErrors => {
          // binding failure, you retrieve the form containing errors:
          BadRequest(views.html.registerForm(formWithErrors, originUrl, client))
        },
        userData => {
          //TODO: Confirm email before creating account!
          client match {
            case Some(c) =>
              val profile = pac4j.getCurrentProfile.get
              createIdentity(
                userData.username,
                userData.password,
                userData.email,
                s"${profile.getFirstName} ${profile.getFamilyName}",
                Some((c, profile.getId)))
            case None =>
              createIdentity(userData.username,
                             userData.password,
                             userData.email)
          }
          Redirect(routes.AuthController.portal(originUrl).url)
        }
      )
    }

  private def doCallback(originUrl: String)(implicit request: RequestHeader) = {
    Try {
      val accessCode = createAccessCode()
      Redirect(returnUrl(originUrl, accessCode))
    } match {
      case Failure(e) =>
        logger.warn("Error while callbacking", e)
        ServerErrors.ISE("Error while redirecting")
      case Success(redirect) => redirect
    }
  }

  /** Form processing for login process (triggered by consolidation process only)
    * Note that the case of logging in using the default client is handled by pac4j.
    *
    * @param originUrl url the was being accessed when the security hook was triggered
    * @param client which client triggered the registration process (in case of identity consolidation). None if user creates account manually
    * @return
    */
  def loginSubmit(originUrl: String, client: String) =
    messagesAction { implicit request =>
      LoginForm.form.bindFromRequest.fold(
        formWithErrors => {
          // binding failure, you retrieve the form containing errors:
          BadRequest(
            views.html.loginForm(formWithErrors, originUrl, Some(client)))
        },
        userData => {
          val webContext = new PlayWebContext(request, playSessionStore)
          //We check the credentials
          val creds = new UsernamePasswordCredentials(userData.username,
                                                      userData.password)
          mongoProfileService.validate(creds, webContext)
          val localProfile = creds.getUserProfile
          val profile = pac4j.getCurrentProfile.get
          //The credentials exist and is valid
          //Then we get the matching identity
          mongo.findOneWithFieldAs[Identity](
            mongo.Collections.identities,
            localProfile.getId,
            s"linkedIds.$defaultClient") match {
            case Some(identity) =>
              val updatedIdentity = identity.copy(
                email =
                  if (identity.email.isEmpty) profile.getEmail
                  else identity.email,
                name =
                  if (identity.name.isEmpty)
                    s"${profile.getFirstName} ${profile.getFamilyName}"
                  else identity.name,
                linkedIds = identity.linkedIds.get(client) match {
                  case Some(_) =>
                    throw new IllegalStateException(
                      s"Attempt to override an $client linked id")
                  case None =>
                    identity.linkedIds ++ Map(client -> profile.getId)
                }
              )
              val map = Map[String, Option[AnyRef]](
                "email" -> (if (identity.email.isEmpty)
                              Some(profile.getEmail)
                            else None),
                "name" -> (if (identity.name.isEmpty)
                             Some(
                               s"${profile.getFirstName} ${profile.getFamilyName}")
                           else None),
                s"linkedIds.$client" -> (identity.linkedIds
                  .get(client) match {
                  case Some(_) =>
                    throw new IllegalStateException(
                      s"Attempt to override an $client linked id")
                  case None =>
                    Some(profile.getId)
                })
              )
              mongo.update(mongo.Collections.identities,
                           identity.id,
                           "id",
                           map.collect {
                             case (k, v) if v.nonEmpty => k -> v.get
                           })
              doCallback(originUrl)
            case None =>
              //No identity was found
              throw new Exception(
                s"Identity for local '${userData.username}' not found")
          }
        }
      )
    }

  /** Retrieve identity against temporary access code
    *
    * @param accessCode
    * @return
    */
  def retrieveIdentity(accessCode: String) =
    Secure("ClientBackendDirectClient").andThen(messagesAction) {
      implicit request: MessagesRequestHeader =>
        val oCodeDoc =
          mongo.findOneAndDeleteWithField(
            mongo.Collections.accessCodes,
            accessCode,
            "code") //The access code is deleted if found
        oCodeDoc match {
          case Some(codeDoc) =>
            val id = codeDoc.getString("id")
            val oIdentity =
              mongo.findOneWithField(mongo.Collections.identities, id, "id")
            oIdentity match {
              case Some(identity) =>
                Ok(identity.toJson())
              case None =>
                logger.warn(
                  s"Attempted to retrieve identity '$id' which does not exist")
                ServerErrors.ISE("Identity does not exist")
            }
          case None =>
            logger.info(
              s"Client attempted to retrieve an identity from a non existing access code")
            Forbidden(
              "Could not retrieve identity from provided access code (might have expired)")
        }
    }
}
