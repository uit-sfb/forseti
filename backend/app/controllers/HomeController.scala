package controllers

import javax.inject._
import org.pac4j.core.profile.CommonProfile
import org.pac4j.play.scala.{
  Pac4jScalaTemplateHelper,
  Security,
  SecurityComponents
}
import play.api._
import play.api.mvc._

/**
  * This controller creates an `Action` to handle HTTP requests to the
  * application's home page.
  */
@Singleton
class HomeController @Inject()(
    messagesAction: MessagesActionBuilder,
    val controllerComponents: SecurityComponents,
    implicit val pac4j: Pac4jScalaTemplateHelper[CommonProfile],
    implicit val cfg: play.api.Configuration)
    extends Security[CommonProfile] {
  val logger = Logger(this.getClass)

  def index() = Action { implicit request: Request[AnyContent] =>
    Ok(views.html.index())
  }

  def api() = Action { implicit request: Request[AnyContent] =>
    logger.info("Redirecting to swagger generated API")

    Redirect(
      s"http://${request.host}/docs/swagger-ui/index.html?url=/assets/swagger.json")
  }
}
