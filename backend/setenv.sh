#!/usr/bin/env bash

export MONGODB_FORSETI_PASSWORD=$(kubectl get secret --namespace default mongodb -o jsonpath="{.data.mongodb-root-password}" | base64 --decode)