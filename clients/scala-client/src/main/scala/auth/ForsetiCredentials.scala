package auth

import org.pac4j.core.credentials.Credentials

//Cannot use Option here because of Java deserialization issue in JavaSerializationHelper (class not trusted)
case class ForsetiCredentials(id: String,
                              username: String,
                              email: String,
                              name: String)
    extends Credentials
