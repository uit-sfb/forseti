package auth

import org.pac4j.core.context.{Pac4jConstants, WebContext}
import org.pac4j.core.credentials.authenticator.Authenticator
import org.pac4j.core.profile.CommonProfile
import org.pac4j.core.profile.definition.CommonProfileDefinition

class ForsetiAuthenticator extends Authenticator[ForsetiCredentials] {
  override def validate(credentials: ForsetiCredentials,
                        context: WebContext): Unit = {
    val profile = new CommonProfile()
    profile.setId(credentials.id)
    profile.addAttribute(Pac4jConstants.USERNAME, credentials.username)
    profile.addAttribute(CommonProfileDefinition.EMAIL, credentials.email)
    profile.addAttribute(CommonProfileDefinition.DISPLAY_NAME, credentials.name)
    credentials.setUserProfile(profile)
  }
}
