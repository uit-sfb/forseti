package auth

import java.net.URLEncoder

import org.pac4j.core.context.WebContext
import org.pac4j.core.logout.LogoutActionBuilder
import org.pac4j.core.profile.CommonProfile
import org.pac4j.core.redirect.RedirectAction

class ForsetiLogoutActionBuilder(forsetiEndpoint: String)
    extends LogoutActionBuilder[CommonProfile] {
  override def getLogoutAction(context: WebContext,
                               currentProfile: CommonProfile,
                               targetUrl: String): RedirectAction = {
    val baseAddr =
      s"${context.getScheme}://${context.getServerName}:${context.getServerPort}"
    val encodedTargetUrl =
      URLEncoder.encode(s"$baseAddr${targetUrl.drop(7)}", "utf-8") //We remove http:// from targetUrl
    val url = s"$forsetiEndpoint/logout?url=$encodedTargetUrl"
    RedirectAction.redirect(url)
  }
}
