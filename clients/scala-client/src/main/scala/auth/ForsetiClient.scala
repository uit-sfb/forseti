package auth

import java.net.URLEncoder

import org.pac4j.core.client.IndirectClient
import org.pac4j.core.context.WebContext
import org.pac4j.core.credentials.authenticator.Authenticator
import org.pac4j.core.credentials.extractor.CredentialsExtractor
import org.pac4j.core.logout.NoLogoutActionBuilder
import org.pac4j.core.profile.CommonProfile
import org.pac4j.core.profile.creator.{
  AuthenticatorProfileCreator,
  ProfileCreator
}
import org.pac4j.core.redirect.{RedirectAction, RedirectActionBuilder}
import play.api.libs.ws.WSClient

import scala.concurrent.ExecutionContext

class ForsetiClient[U <: CommonProfile](
    forsetiEndpoint: String,
    forsetiClientKey: String,
    forsetiClientSecret: String,
    authenticator: Authenticator[ForsetiCredentials],
    profileCreator: ProfileCreator[ForsetiCredentials, U] =
      new AuthenticatorProfileCreator[ForsetiCredentials, U](),
    redirectActionBuilder: Option[RedirectActionBuilder] = None,
    credentialsExtractor: Option[CredentialsExtractor[ForsetiCredentials]] =
      None)(implicit ws: WSClient, ec: ExecutionContext)
    extends IndirectClient[ForsetiCredentials, U] {
  override def clientInit(): Unit = {
    defaultRedirectActionBuilder(redirectActionBuilder.getOrElse(
      new RedirectActionBuilder {
        override def redirect(context: WebContext): RedirectAction = {
          val callbackUrl: String = computeFinalCallbackUrl(context)
          def encode(m: Map[String, String]): String = {
            val encoding = "utf-8"
            (m map {
              case (k, v) =>
                s"${URLEncoder.encode(k, encoding)}=${URLEncoder.encode(v, encoding)}"
            }).mkString("&")
          }
          val params = Map("callback" -> callbackUrl)
          RedirectAction.redirect(
            getUrlResolver.compute(s"$forsetiEndpoint/portal?${encode(params)}",
                                   context))
        }
      }
    ))
    defaultCredentialsExtractor(
      credentialsExtractor.getOrElse(
        new ForsetiCredentialsExtractor(forsetiEndpoint,
                                        forsetiClientKey,
                                        forsetiClientSecret)))
    defaultAuthenticator(authenticator)
    defaultProfileCreator(profileCreator)
    defaultLogoutActionBuilder(new NoLogoutActionBuilder())
  }
}
