package auth

import org.pac4j.core.context.WebContext
import org.pac4j.core.credentials.extractor.CredentialsExtractor
import play.api.libs.ws.{WSAuthScheme, WSClient}

import scala.concurrent.duration._
import scala.concurrent.{Await, ExecutionContext}

class ForsetiCredentialsExtractor(
    authEndpoint: String,
    clientName: String,
    clientPassword: String)(implicit ws: WSClient, ec: ExecutionContext)
    extends CredentialsExtractor[ForsetiCredentials] {
  override def extract(context: WebContext) = {
    val accessCodeFieldName = "access_code"
    val accessCode = context.getRequestParameter(accessCodeFieldName)
    if (accessCode != null) {
      val request = ws
        .url(s"$authEndpoint/retrieveId")
        .addQueryStringParameters(accessCodeFieldName -> accessCode)
        .addHttpHeaders("Accept" -> "application/json")
        .withAuth(clientName, clientPassword, WSAuthScheme.BASIC)
      val fCreds = request.get() map { response =>
        if (response.status >= 400) {
          throw new Exception(
            s"Oops, something went wrong while trying to retrieve identity from access code! Error code: ${response.status} (${response.statusText}): ${response.body}")
        } else {
          val id = (response.json \ "id").as[String]
          val username =
            (response.json \ "username").as[String]
          val email = (response.json \ "email").as[String]
          val name = (response.json \ "name").asOpt[String]
          ForsetiCredentials(id, username, email, name.getOrElse(""))
        }
      }
      Await.result[ForsetiCredentials](fCreds, 30.seconds)
    } else
      throw new Exception(s"URL parameter '$accessCodeFieldName' missing")
  }
}
