# Forseti scala client

The scala client is based on [pac4j](http://www.pac4j.org/) and can be used by Scala and Java [Play](https://www.playframework.com/) applications alike.

## Usage

In your client application's `build.sbt`, add:
  - `resolvers += "Artifactory" at "https://artifactory.metapipe.uit.no/artifactory/sbt-release-local/"`
  - Add the dependencies: 
```
libraryDependencies += "org.pac4j" % "pac4j-http" % "3.7.0"
libraryDependencies += "no.uit.sfb" %% "forseti-client-scala" % "0.1.2",
libraryDependencies += "org.apache.shiro" % "shiro-core" % "1.4.0", //needed at runtime by SecurityModule
```

In case your application is a Java project, you should add the dependencies above to Maven (with the maven syntax) and add the Scala library too:
```
<dependency>
    <groupId>org.scala-lang</groupId>
    <artifactId>scala-library</artifactId>
    <version>2.12.8</version>
</dependency>
```

  - Add the callback and logout routes:  
```
GET     /callback                   @org.pac4j.play.CallbackController.callback()
POST    /callback                   @org.pac4j.play.CallbackController.callback()
GET     /logout                     @org.pac4j.play.LogoutController.logout()
```
  - Add modules/SecurityModule.scala as described in [play-pac4j doc](https://github.com/pac4j/play-pac4j/wiki/Security-configuration) and add:
```
@Provides
def provideForsetiClient(
    implicit ws: WSClient,
    ec: ExecutionContext
): ForsetiClient[CommonProfile] = {
  val cl = new ForsetiClient[CommonProfile](
    configuration.get[String]("app.auth.forseti.url"),
    configuration.get[String]("app.auth.forseti.client.key"),
    configuration.get[String]("app.auth.forseti.client.secret"),
    new ForsetiAuthenticator()
  )
  cl.setLogoutActionBuilder(
    new ForsetiLogoutActionBuilder(
      configuration.get[String]("app.auth.forseti.url")))
  cl
}
```
  - Make sure you configure the logoutController with:
```
logoutController.setCentralLogout(true)
logoutController.setDefaultUrl("http://" + "/")
```
  - Make sure the client config uses the default resolver with complete relativeUrl = true: `clients.setUrlResolver(new DefaultUrlResolver(true))`
  - In your controllers, secure any protected method with `Secure("ForsetiClient") {...}`
  - In `reference.conf`, add: `play.modules.enabled += "modules.SecurityModule"`
  - In `application.conf`, set up:
    - `app.auth.forseti.url`: Endpoint of the forseti instance (including `https://` or `http://` and the port)
    - `app.auth.forseti.client.key`: a client key you get from your Forseti admin
    - `app.auth.forseti.client.secret`: a client secret you get from your Forseti admin
  
## Publish

`sbt publish`