# Forseti clients

## List of clients

- scala-client: a client for Scala and Java

Note: admin-client is for administration and cannot be used in an application client.

## Forseti specification

Forseti provides client application with a delegated indirect authentication mechanism.
Any indirect authentication procedures contains two steps: redirect (where the client application hands over to the delegate auth service), 
and callback (where the client application retrieve the user's information if the process was successful).

![Forseti client spec](source/images/schematics/forseti_client_spec.png)

1. Redirect  
When a protected route requires authentication, redirect to: `<forsetiEndpoint>/portal?callback=<yourCallbackUrl>`.
Note that `<yourCallbackUrl>` must be URL encoded.
1. Callback: When the authentication process completes successfully, Forseti redirects to `<yourCallbackUrl>&access_code=<accessCode>`.
    - Perform: GET `<forsetiEndpoint>/<retrieveId>?access_code=<accessCode>` with basic auth (the credentials should be asked to your Forseti admin)
    - Parse Forseti's answers (cf. below) to get the user's information.
```json
{
  "id": <id>,
  "email": <email>,
  "name": <name>,
  "username": <username>
}
```  