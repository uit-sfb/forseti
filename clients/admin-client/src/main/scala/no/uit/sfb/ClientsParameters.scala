package no.uit.sfb

case class ClientsParameters(cmd: String = "",
                             clientName: String = "",
                             clientSecret: String = "")
