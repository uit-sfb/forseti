package no.uit.sfb

import com.mongodb.{
  MongoClient,
  MongoClientOptions,
  MongoCredential,
  ServerAddress
}
import org.apache.shiro.authc.credential.DefaultPasswordService
import org.pac4j.core.credentials.password.ShiroPasswordEncoder
import org.pac4j.mongo.profile.service.MongoProfileService
import com.typesafe.config.{Config, ConfigFactory}
import no.uit.sfb.forseti.pac4j.patches.MongoProfileServicePatch
import org.pac4j.core.context.Pac4jConstants
import org.pac4j.mongo.profile.MongoProfile
import scopt.OParser

object Main extends App {
  val config: Config = ConfigFactory.load()
  lazy val mongoClient = {
    val credential = MongoCredential.createCredential(
      config.getString("db.mongodb.user"),
      "admin",
      config.getString("db.mongodb.password").toCharArray)
    new MongoClient(
      new ServerAddress(config.getString("db.mongodb.host"),
                        config.getInt("db.mongodb.port")),
      credential,
      MongoClientOptions.builder().build()
    )
  }
  lazy val mongoProfileService = {
    val passwordEncoder = new ShiroPasswordEncoder(new DefaultPasswordService())
    val profileService =
      new MongoProfileServicePatch(mongoClient, "", passwordEncoder)
    profileService.setIdAttribute("_id")
    profileService.setUsernameAttribute(
      config.getString("app.usernameAttribute"))
    profileService.setUsersCollection(config.getString("app.collectionName"))
    profileService.setUsersDatabase(config.getString("app.databaseName"))
    profileService
  }

  val p = getClass.getPackage
  val name = p.getImplementationTitle
  val ver = p.getImplementationVersion

  val mainBuilder = OParser.builder[String]

  val mainParser = {
    import mainBuilder._
    OParser.sequence(
      programName(name),
      head(name, ver),
      help('h', "help")
        .text("prints this usage text"),
      version('v', "version")
        .text("prints the version"),
      cmd("clients")
        .action((_, _) => "clients")
        .text("Manage of clients")
    )
  }

  val clientsBuilder = OParser.builder[ClientsParameters]

  val clientsParser = {
    import clientsBuilder._
    OParser.sequence(
      cmd("add")
        .action((_, c) => c.copy(cmd = "add"))
        .text("Add client")
        .children(
          arg[String]("<client-name>")
            .action((x, c) => c.copy(clientName = x))
            .text("Client name"),
          arg[String]("<client-secret>")
            .action((x, c) => c.copy(clientSecret = x))
            .text("Client secret")
        ),
      cmd("remove")
        .action((_, c) => c.copy(cmd = "remove"))
        .text("Remove client")
        .children(
          arg[String]("<client-name>")
            .action((x, c) => c.copy(clientName = x))
            .text("Client name")
        )
    )
  }

  (OParser.parse(mainParser, args.headOption.toSeq, ""),
   if (args.nonEmpty) args.tail else Array("-h")) match {
    case (Some("clients"), arg) =>
      OParser.parse(clientsParser, arg, ClientsParameters()) match {
        case Some(params) =>
          lazy val profile = {
            val p = new MongoProfile()
            p.setId(params.clientName)
            p.addAttribute(Pac4jConstants.USERNAME, params.clientName)
            p
          }
          params.cmd match {
            case "add" =>
              mongoProfileService.create(profile, params.clientSecret)
            case "remove" =>
              mongoProfileService.remove(profile)
            case x =>
              throw new Exception(s"Unknown command '$x'. Use '-h' for help.")
          }
        case _ => // arguments are bad, error message will have been displayed
      }
    case _ =>
    // arguments are bad, error message will have been displayed
  }
}
