import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.core.config.Configurator
import org.apache.logging.log4j.Level._
import NativePackagerHelper._

name := "forseti-admin-client"
organization := "no.uit.sfb"
logLevel := {
  Configurator.setAllLevels(LogManager.getRootLogger.getName, INFO)
  sbt.util.Level.Info
}

maintainer := "mmp@uit.no"

ThisBuild / credentials += Credentials(Path.userHome / ".sbt" / ".credentials")

ThisBuild / publishTo := {
  if ((ThisBuild / version).value.endsWith("-SNAPSHOT"))
    Some("Artifactory Realm" at "https://artifactory.metapipe.uit.no/artifactory/sbt-dev-local;build.timestamp=" + new java.util.Date().getTime)
  else
    Some("Artifactory Realm" at "https://artifactory.metapipe.uit.no/artifactory/sbt-release-local")
}

ThisBuild / resolvers ++= {
  Seq(Some("Artifactory" at "https://artifactory.metapipe.uit.no/artifactory/sbt-release-local/"),
    if ((ThisBuild / version).value.endsWith("-SNAPSHOT"))
      Some("Artifactory-dev" at "https://artifactory.metapipe.uit.no/artifactory/sbt-dev-local/")
    else
      None
  ).flatten
}

lazy val root = (project in file("."))

enablePlugins(UniversalPlugin, JavaAppPackaging, ReleaseArtifactory)

enablePlugins(GitVersioning)
useJGit
git.gitlabCiOverride := true
git.targetVersionFile := "../../targetVersion"

mappings in Universal ++= directory("conf")
topLevelDirectory := Some(packageName.value)

Artifactory / artifBuildArtifacts := Seq((Universal / packageBin).value.toPath)
Artifactory / artifProjectVersion := version.value
Artifactory / artifLocation := s"${organization.value}/forseti/${name.value}"

scalaVersion := "2.12.8"
val pac4jVersion = "3.7.0"

libraryDependencies ++= Seq(
  "org.pac4j" %% "play-pac4j" % "8.0.0",
  "org.pac4j" % "pac4j-mongo" % pac4jVersion,
  "org.apache.shiro" % "shiro-core" % "1.4.0",
  "com.typesafe" % "config" % "1.3.4",
  "com.github.scopt" %% "scopt" % "4.0.0-RC2",
  "com.typesafe.scala-logging" %% "scala-logging" % "3.9.0",
  "ch.qos.logback" % "logback-classic" % "1.2.3",
  "no.uit.sfb" %% "forseti-common" % version.value
)

enablePlugins(BuildInfoPlugin)
buildInfoKeys := Seq[BuildInfoKey](name, version, scalaVersion, sbtVersion)
buildInfoPackage := s"${organization.value}.info.${name.value.replace('-', '_')}"
