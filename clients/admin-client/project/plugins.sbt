addSbtPlugin("com.eed3si9n" % "sbt-buildinfo" % "0.9.0")
addSbtPlugin("com.typesafe.sbt" % "sbt-native-packager" % "1.3.19")
addSbtPlugin("no.uit.sfb" % "artifactory-sbt-plugin" % "0.1.0")
addSbtPlugin("no.uit.sfb" % "sbt-git-smartversion" % "1.1.0")
