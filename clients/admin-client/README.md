# Forseti admin-client

Each time a new application client wants to use Forseti service a new set of client key/secret needs to be added to the `clients` collection.
`admin-client` is a tool to perform such administrative tasks.

## Usage

- Download the .zip from Artifactory and unzip it: 
```[bash]
curl https://artifactory.metapipe.uit.no/artifactory/generic-local/no.uit.sfb/forseti/forseti-admin-client/0.1.2/forseti-admin-client-0.1.2.zip > forseti-admin-client.zip \
 && unzip forseti-admin-client.zip \
 && rm forseti-admin-client.zip \
 && cd forseti-admin-client
```
- Adapt mongodb settings to match your system in conf/app.conf (start with `include "application"` to use the default values).

### Adding a client

- Start by generating a `clientName` and `clientSecret`. The `clientName` may be a descriptive string, while the `clientSecret` must be a random string.
- Add client to database: `bin/forseti-admin-client -Dconfig.file=conf/app.conf -- clients add <clientName> <clientSecret>`
- Return the key/secret pair to the administrator of the client application in a safe way.

### Deleting a client

`bin/forseti-admin-client -Dconfig.file=conf/app.conf -- clients remove <clientName>`

After this action, the application client will not be able to use Forseti service.


## Packaging and publishing

### Packaging

To build the .zip package (in target): `sbt universal:packageBin`

### Publish to Artifactory

`artifactory:publish`

## Setup

In case Mongodb is deployed with the Helm chart, run the following:

```[bash]
. setenv.sh #Note that it is NOT `./setenv.sh`
```
