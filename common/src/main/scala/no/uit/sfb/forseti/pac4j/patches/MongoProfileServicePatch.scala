package no.uit.sfb.forseti.pac4j.patches

import java.util

import com.mongodb.MongoClient
import com.mongodb.client.model.Filters
import org.bson.Document
import org.pac4j.core.credentials.password.PasswordEncoder
import org.pac4j.core.profile.service.AbstractProfileService
import org.pac4j.core.util.CommonHelper
import org.pac4j.mongo.profile.service.MongoProfileService

class MongoProfileServicePatch(mongoClient: MongoClient = null,
                               attributes: String = null,
                               passwordEncoder: PasswordEncoder)
    extends MongoProfileService(mongoClient, attributes, passwordEncoder) {
  override def deleteById(id: String): Unit = {
    logger.debug("Delete id: {}", id)
    getCollection.deleteOne(Filters.eq(getIdAttribute, id))
  }

  override def update(attributes: util.Map[String, AnyRef]): Unit = {
    import scala.collection.JavaConverters._

    val attrs = attributes.asScala.toMap

    val id = attrs
      .collectFirst { case (k, v) if k == getIdAttribute => v }
      .get
      .asInstanceOf[String] //We need to know the id to be able to update the document

    val doc = new Document

    attrs foreach {
      case (name, value) =>
        if (name != getIdAttribute)
          doc.append(name, value)
    }

    CommonHelper.assertNotNull(AbstractProfileService.ID, id)
    logger.debug(s"Updating id: $id with doc: $doc")
    getCollection.updateOne(Filters.eq(getIdAttribute, id),
                            new Document("$set", doc))
  }
}
