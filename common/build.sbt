import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.core.config.Configurator
import org.apache.logging.log4j.Level._

name := "forseti-common"
organization := "no.uit.sfb"
scalaVersion := "2.12.8"
logLevel := {
  Configurator.setAllLevels(LogManager.getRootLogger.getName, INFO)
  sbt.util.Level.Info
}

ThisBuild / credentials += Credentials(Path.userHome / ".sbt" / ".credentials")

ThisBuild / publishTo := {
  if ((ThisBuild / version).value.endsWith("-SNAPSHOT"))
    Some("Artifactory Realm" at "https://artifactory.metapipe.uit.no/artifactory/sbt-dev-local;build.timestamp=" + new java.util.Date().getTime)
  else
    Some("Artifactory Realm" at "https://artifactory.metapipe.uit.no/artifactory/sbt-release-local")
}

ThisBuild / resolvers ++= {
  Seq(Some("Artifactory" at "https://artifactory.metapipe.uit.no/artifactory/sbt-release-local/"),
    if ((ThisBuild / version).value.endsWith("-SNAPSHOT"))
      Some("Artifactory-dev" at "https://artifactory.metapipe.uit.no/artifactory/sbt-dev-local/")
    else
      None
  ).flatten
}

lazy val root = (project in file("."))

enablePlugins(GitVersioning)
useJGit
git.gitlabCiOverride := true
git.targetVersionFile := "../targetVersion"

val pac4jVersion = "3.7.0"

libraryDependencies ++= Seq(
  "org.pac4j" % "pac4j-mongo" % pac4jVersion,
  "com.typesafe.scala-logging" %% "scala-logging" % "3.9.0",
  "ch.qos.logback" % "logback-classic" % "1.2.3" % Test
)